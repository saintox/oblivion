<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table    = 'control';
    protected $fillable = ['device_id', 'username', 'value1', 'value2', 'value3','value4'];
    protected $primaryKey = 'id';
}
