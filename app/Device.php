<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table    = 'device';
    protected $fillable = ['username', 'device_id','generated_id'];
    protected $primaryKey = 'id';
}
