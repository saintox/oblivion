<?php

namespace App\Http\Controllers;

use App\Device;
use App\Data;
use App\User;
use App\Control;
use Request;
use Route;
use Session;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Input;
use Redirect;

class MonitorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function index (request $request)
    {
        $userdev = Device::where('username', '=', Auth::user()->username)->get();
        for ($i=0; $i<count($userdev); $i++)
        {
            $arrname[$i] = $userdev[$i]->device_id;
        }
        $devlist = array_values(array_unique($arrname));

        $input=Input::get('sensor');
        if ($input)
        {
            $act = 1;
            $criteria =['username'=>Auth::user()->username, 'device_id'=>$input];            
            $selected = Data::where($criteria)->get();
            $data =json_encode($selected);
            for ($t=1; $t<=3; $t++)
            {
                $test = array_column(json_decode(json_encode($selected), True),'sensor'.$t);
                if (array_filter($test))
                {
                    $stat[$t]=1;
                } else $stat[$t]=0;
            }
        }
        else
        {
            $input = NULL;
            $stat = NULL;
            $act = NULL;
            $data = 1;
            $selected = 1;
        }

        $getcontrol = Control::where ([['username','=',Auth::user()->username],['device_id','=',$input]])->first();
        for ($i=1; $i<=3; $i++)
        {
            $control[$i] = $getcontrol['value'.$i];
        }
        return view('main.monitor')->with('input',$input)->with('control',$control)->with ('data', $data)->with ('selected',$selected)->with ('devlist',$devlist)->with('act',$act)->with('stat',$stat);
    }

    protected function update (request $request)
    {
        $devac = Input::get('activedevice');
        $trigger = Input::all();
        for ($i=1; $i<=3; $i++)
        {
            if (isset($trigger['trigger'.$i]))
            {
                Control::where([['username','=',Auth::user()->username],['device_id','=',$devac]])->update(['value'.$i=>$trigger['trigger'.$i]]);
                return redirect()->route('home');
            }
        }
        
    }
}
