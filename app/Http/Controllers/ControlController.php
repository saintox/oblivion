<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\User;
use App\Control;
use Route;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;

class ControlController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    protected function control(request $request)
    {
        $unique = User::all();
        $devid = $request->input('id');
        $operand = $request->input('op');
        $datadevice = Device::all();
        
        // operasi xor
        for ($i=0; $i<count($datadevice); $i++)
        {
            if (strlen((string)$devid) == strlen((String)$datadevice[$i]->generated_id))
            {
                $temp = (string) $datadevice[$i]->generated_id;
                for ($j=0; $j<strlen((string)$devid); $j++)
                {
                    if ((($devid[$j] - $temp[$j])==0) || (($devid[$j] - $temp[$j])==1))
                    {
                        $check[$j] = $devid[$j] - $temp[$j];
                    } 
                    else 
                    {
                        unset($check);
                        $j=strlen((string)$devid);
                    }
                }
            } else unset($check);
            
            if(isset($check))
            {
                $counter = $check[0]; 
                for ($i=1; $i<count($check); $i++)
                {
                        $counter = $counter ^ $check[$i];
                }
            } else $counter = 2;

            if ($operand == $counter)
            {
                $devid = $temp;
                break;
            }
        }

        $k = 0;
        foreach ($unique as $u)
        {
            $data = $u['unique_id'];
            // proses kalkulasi untuk mendapatkan numerik device_id
            $j=0;
            for ($i=0; $i < strlen((string)$devid); $i++)
            {
                if (isset($data[$j]))
                {
                    if (isset($devid[$i+1]) && $devid[$i]-$data[$j]<0 )
                    {
                        $hasil[$j] = ($devid[$i].$devid[$i+1]) - $data[$j];
                        $i++;
                    }
                    else
                    $hasil[$j] = $devid[$i] - $data[$j];
                    $j++;
                }   
            }
            $unik[$k] = $hasil;
            $k++;
        }
        // return $unik;
        
        // proses pengambilan semua device id + diiterasikan ke numerik
        $device = Device::all();
        $iterate = range ('a','z');
        $j=0;
        foreach ($device as $d)
        {
            $device_id = $d['device_id'];
            for ( $i=0 ; $i < strlen($device_id); $i++ )
            {
                $output[$i] = array_search($device_id[$i],$iterate);
            }
            
            $id[$j] = $output;
            $j++;
        }
        // return $id;
        
        // proses pengecekan ada pada indeks berapa aja array yang sama
        for ($k=0; $k<count($unik); $k++)
        {
            if (in_array ($unik[$k],$id))
            {
                $index['id'] = array_keys($id,$unik[$k]);
                $index['username'] = $unique[$k]->username;
                break;
            } 
            else $index = NULL;
        }
        // return $index;
        
        if (isset($index))
        {
            foreach ($index['id'] as $i)
            {
                if (($i+1 == $device[$i]->id) && ($index['username'] == $device[$i]->username))
                {
                    //  catch input get
                    $match =['username'=>$device[$i]->username, 'device_id'=>$device[$i]->device_id];

                    $status = Control::where($match)->get();
                    echo "$";
                    echo $status[0]->value1;
                    echo $status[0]->value2;
                    echo $status[0]->value3;
                }
            }
        } else return "gak bisa gak ketemu";
    }

}