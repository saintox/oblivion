<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Data;
use App\User;
use App\Control;
use Route;
use Session;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Input;

class AddDataController extends Controller
{

    protected function add(request $request)
    {
        // olah waktu
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        date_default_timezone_set('Asia/Jakarta');
        $time = new DateTime( date('H:i:s.'.$micro, $t) );
        $unique = User::all();
        
        $devid = $request->input('id');
        $k = 0;
        $operand = $request->input('op');
        $datadevice = Device::all();
        
        
        // search + operasi xor
        for ($i=0; $i<count($datadevice); $i++)
        {
            // cari yang panjangnya sama di db
            if (strlen((string)$devid) == strlen((String)$datadevice[$i]->generated_id))
            {
                $temp = (string) $datadevice[$i]->generated_id;
                for ($j=0; $j<strlen((string)$devid); $j++)
                {
                    // kalau di minus = 0/1 berarti dilanjut, selain itu ignored 
                    if ((($devid[$j] - $temp[$j])==0) || (($devid[$j] - $temp[$j])==1))
                    {
                        $check[$j] = $devid[$j] - $temp[$j];
                    } 
                    else 
                    {
                        unset($check);
                        $j=strlen((string)$devid);
                    }
                }
            } else unset($check);
            
            if(isset($check))
            {
                $counter = $check[0]; 
                for ($i=1; $i<count($check); $i++)
                {
                    $counter = $counter ^ $check[$i];
                }
            } else $counter = 2;

            if ($operand == $counter)
            {
                $devid = $temp;
                break;
            }
        }

        foreach ($unique as $u)
        {
            $data = $u['unique_id'];
            // proses kalkulasi untuk mendapatkan numerik device_id
            $j=0;
            for ($i=0; $i < strlen((string)$devid); $i++)
            {
                if (isset($data[$j]))
                {
                    if (isset($devid[$i+1]) && $devid[$i]-$data[$j]<0 )
                    {
                        $hasil[$j] = ($devid[$i].$devid[$i+1]) - $data[$j];
                        $i++;
                    }
                    else
                    $hasil[$j] = $devid[$i] - $data[$j];
                    $j++;
                }   
            }
            $unik[$k] = $hasil;
            $k++;
        }
        
        // proses pengambilan semua device id + diiterasikan ke numerik
        $device = Device::all();
        $iterate = range ('a','z');
        $j=0;
        foreach ($device as $d)
        {
            $device_id = $d['device_id'];
            for ( $i=0 ; $i < strlen($device_id); $i++ )
            {
                $output[$i] = array_search($device_id[$i],$iterate);
            }
            
            $id[$j] = $output;
            $j++;
        }
        // return $id;
        
        // proses pengecekan ada pada indeks berapa aja array yang sama
        for ($k=0; $k<count($unik); $k++)
        {
            if (in_array ($unik[$k],$id))
            {
                $index['id'] = array_keys($id,$unik[$k]);
                $index['username'] = $unique[$k]->username;
                break;
            } 
            else $index = NULL;
        }
        // return $index;

        if (isset($index))
        {
            // $i=1;
            foreach ($index['id'] as $i)
            {                
                if (($i+1 == $device[$i]->id) && ($index['username'] == $device[$i]->username))
                {
                    // input database disini
                    $analog1 = $request->input('analog1');
                    $digital1 = $request->input('digital1');
                    $digital2 = $request->input('digital2');
                    $analog1_hasil = (($analog1/1024)*3300)/10;
                    if ($digital1 < 2)
                    {
                        $digital1 = 0;
                    }

                    $waktu = $time->format("H:i:s");
                    $waktu_akurat = $time->format("H:i:s.u");
                    
                    // check control kalau 0 dia gak dimasukin
                    $status = Control::where([['username', '=', $device[$i]->username],['device_id', '=', $device[$i]->device_id]])->first();
                    if (isset($status))
                    {
                        if ($status['value1']==NULL)
                        {
                            $analog1_hasil = NULL;
                        } 
                        else if ($status['value2']==NULL)
                        {
                            $digital1 = NULL;
                        }
                        else if ($status['value3']==NULL)
                        {
                            $digital2 = NULL;
                        }
                    }

                    $input = new Data;
                    $input['username']      = $device[$i]->username;
                    $input['device_id']     = $device[$i]->device_id;
                    $input['waktu']         = $waktu;
                    $input['waktu_akurat']  = $waktu_akurat;
                    $input['sensor1']       = $analog1_hasil;
                    $input['sensor2']       = $digital1;
                    $input['sensor3']       = $digital2;
                    $input->save(); 
                    return "alhamdulillah";
                }
                // $i++;
            }
        } else return "gak bisa gak ketemu";
        // kalau mau buat API disini, nanti ID yang tidak terbaca dianggap ada serangan masuk
        // return 'success input alhamdulillah';       
    }
}
