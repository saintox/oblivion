@include('layouts.navbar')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Device</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" method="GET" action="{{ route('submitdevice') }}">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <input type="hidden" id="unique_id" name="unique_id" value="0">
                        </div>
                       
                        <div class="form-group">
                            <label for="username" class="col-md-4 control-label">Username</label>
                            <div class="col-md-6">
                                <input id="username" type="text" value="{{Auth::user()->username}}" class="form-control" name="username" readonly="true">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input_id" class="col-md-4 control-label">Device ID</label>

                            <div class="col-md-6">
                                <input id="input_id" type="text" class="form-control" name="input_id" placeholder="insert ID here" required>
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="device_id" class="col-md-4 control-label">Generated Device ID</label>
                            <div class="col-md-6">
                                <input id="device_id" type="text" class="form-control" onclick="generateid()" name="device_id" placeholder="your device ID here" readonly="true" required>
                            </div>
                        </div>
<!--
                        <div class="form-group">
                            <label for="device_id" class="col-md-4 control-label">Device Type</label>
                                <div class="col-md-6">
                                    <select id="device_type" name="device_type" class="form-control" >
                                        <option value=""></option>
                                        <option value="nodemcu">NodeMCU</option>
                                        <option value="arduino">ArduinoUNO</option>        
                                    </select>    
                                </div>                                     
                        </div>

                        <div class="form-group">
                            <label for="devic_job" class="col-md-4 control-label">Device Job</label>
                            <div class="col-md-6">
                                <select id="device_job" name="device_job" class="form-control" >
                                    <option value=""></option>
                                    <option value="mon">Monitoring</option>
                                    <option value="moncol">Monitoring + Controlling</option>        
                                </select>      
                            </div>                               
                        </div>      
-->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $unique_id = Auth::user()->unique_id ?>
<script type="text/javascript">
function generateid() 
{ 
    var input = document.getElementById("input_id").value; 
    var length = input.length;
    var alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0"];
    var id = ({!! $unique_id !!}).toString(10).split("").map(Number);
    var device_id = document.getElementById('device_id');
    var output = [];
    var unique_id = document.getElementById('unique_id'); 
    for ( i=0 ; i < length; i++ )
    {
        output[i] = alphabet.indexOf(input[i]) + id[i];
    }
    device_id.value=output.join("");
    unique_id.value=output;
} 
</script>