
<body>
<div class="wrapper">
@include('layouts.navbar')
    <div class="main">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    @if (Session::has('failed'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="fa fa-check-circle"></i> {{Session::get('failed')}}
                        </div>
                    @endif
                    
                    @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-check-circle"></i> {{Session::get('success')}}
                    </div>
                    @endif
                    <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-heading">
                                <h2 style="text-align:center">ADD DEVICE</h2>
                            </div>
                            <form action="{{url('/adddevice')}}" method="POST">
                            {{ csrf_field() }}    
                            <div class="panel-body">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> &nbsp; Add Device</button>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-heading">
                                <h2 style="text-align:center">CONTROL DEVICE</h2>
                            </div>
                            <form action="{{url('/home')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> &nbsp; Control</button>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel">
                            <div class="panel-heading">
                                <h2 style="text-align:center">MONITOR DEVICE</h2>
                            </div>
                            <form action="{{url('/monitor')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> &nbsp; Monitor Device</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>    
</body>