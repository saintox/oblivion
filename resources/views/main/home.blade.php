@include('layouts.navbar')
<head>
<meta http-equiv="refresh" content="5">
</head>
<script src="assets/vendor/amcharts/amcharts.js" type="text/javascript"></script>
<script src="assets/vendor/amcharts/serial.js" type="text/javascript"></script>
<script src="assets/vendor/amcharts/themes/dark.js" type="text/javascript"></script>
<script src="assets/vendor/amcharts/amcharts.js" type="text/javascript"></script>
<script src="assets/vendor/amcharts/xy.js" type="text/javascript"></script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                <form action='{{url ('/home')}}' method="GET">
                    {{csrf_field()}}
                    <select name="sensor" id="sensor" onchange="this.form.submit();"  class="form-control" style="height: auto; width: auto">
                        <option disabled selected value="">Choose</option>
                        <?php for ($a=0; $a<count($devlist); $a++)
                        { ?>
                        <option id="<?php $devlist[$a] ?>" value='{{ $devlist[$a] }}'>{{ $devlist[$a] }}</option>
                        <?php }?>
                    </select>
                </form>
                <form action='{{url ('/adddevice')}}' method="POST">
                {{csrf_field()}}
                    <button name ="adddevice" type="submit" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Add Device</button>
                </form>
                </div>
            </div>
            <?php 
            if ($act != NULL)
            {
            for ($i=1; $i<=3; $i++) 
            {
                if ($stat[$i]==1)
                {
             ?>            
            <div class="panel panel-default">
                <div class="panel-title" style="text-align:center">
                    <h3> Sensor{{$i}}</h3>    
                </div>
                <div class="panel-body">
                    <div id="sensor{{$i}}" style="width: 100%; height: 400px;"></div>
                </div>
                <div class="panel-footer" style="text-align:right">
                    <form action='{{url ('/update')}}' method="POST">
                    {{csrf_field()}}    
                    <?php if ($control[$i]==1)  {?>
                        <button name ="trigger{{$i}}" type="submit" value="0" class="btn btn-danger"><i class="fa fa-power-off"></i> &nbsp; Turn Off</button>
                    <?php } else {?>
                        <button name ="trigger{{$i}}" type="submit" value="1" class="btn btn-success"><i class="fa fa-power-off"></i> &nbsp; Turn On</button>
                    <?php } ?>
                    <input type="hidden" value="{{$input}}" name="activedevice">                          
                    </form>
                </div>
            </div>
                <?php }
                } }?>
        </div>
    </div>
</div>

<script>
    var data = {!! $data !!}
    var chart = AmCharts.makeChart("sensor1", {
        "type": "serial",
       
        "theme": "light",
        "dataDateFormat": "H-i-s",
        "dataProvider": data,

        "valueAxes": [{
            "maximum": 100,
            "minimum": 0,
            "axisAlpha": 0,
            "title": "TEMPERATURE",
            "guides": [{
                "fillAlpha": 0.1,
                "fillColor": "#CC0000",
                "lineAlpha": 0,
                "toValue": 120,
                "value": 0
            }, {
                "fillAlpha": 0.1,
                "fillColor": "#0000cc",
                "lineAlpha": 0,
                "toValue": 200,
                "value": 120
            }]
        }],
        "graphs": [{
            "bullet": "round",
            "valueField": "sensor1",
            "balloonText": "<p style='text-align: center;'><b>[[waktu_akurat]]</b><br>Value : <b>[[sensor1]]</b> °C", 
            
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable":false,
            "valueZoomable":true
        },
        "categoryField": "waktu",
        "valueScrollbar":{

        }
    });
</script>

<script>
    var data = {!! $data !!}
    var chart = AmCharts.makeChart("sensor2", {
        "type": "serial",
       
        "theme": "light",
        "dataDateFormat": "H-i-s",
        "dataProvider": data,

        "valueAxes": [{
            "maximum": 100,
            "minimum": 0,
            "axisAlpha": 0,
            "title": "TEMPERATURE",
            "guides": [{
                "fillAlpha": 0.1,
                "fillColor": "#CC0000",
                "lineAlpha": 0,
                "toValue": 120,
                "value": 0
            }, {
                "fillAlpha": 0.1,
                "fillColor": "#0000cc",
                "lineAlpha": 0,
                "toValue": 200,
                "value": 120
            }]
        }],
        "graphs": [{
            "bullet": "round",
            "valueField": "sensor2",
            "balloonText": "<p style='text-align: center;'><b>[[waktu_akurat]]</b><br>Value : <b>[[sensor1]]</b> °C", 
            
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable":false,
            "valueZoomable":true
        },
        "categoryField": "waktu",
        "valueScrollbar":{

        }
    });
</script>

<script>
    var data = {!! $data !!}
    var chart = AmCharts.makeChart("sensor3", {
        "type": "serial",
       
        "theme": "light",
        "dataDateFormat": "H-i-s",
        "dataProvider": data,

        "valueAxes": [{
            "maximum": 100,
            "minimum": 0,
            "axisAlpha": 0,
            "title": "TEMPERATURE",
            "guides": [{
                "fillAlpha": 0.1,
                "fillColor": "#CC0000",
                "lineAlpha": 0,
                "toValue": 120,
                "value": 0
            }, {
                "fillAlpha": 0.1,
                "fillColor": "#0000cc",
                "lineAlpha": 0,
                "toValue": 200,
                "value": 120
            }]
        }],
        "graphs": [{
            "bullet": "round",
            "valueField": "sensor3",
            "balloonText": "<p style='text-align: center;'><b>[[waktu_akurat]]</b><br>Value : <b>[[sensor1]]</b> °C", 
            
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable":false,
            "valueZoomable":true
        },
        "categoryField": "waktu",
        "valueScrollbar":{

        }
    });
</script>
